# Using OpsWorks to deploy Your Website



## Overview
[OpsWorks](https://aws.amazon.com/opsworks/?nc1=h_ls) is a configuration management service, let you automate how servers are configured, deployed, and managed across your Amazon EC2 instnaces or on-premises services 

AWS provide three Opworks types :  
- **AWS Opsworks for Chef Automate** : 
AWS OpsWorks for Chef Automate is a fully managed configuration management service that hosts Chef Automate. 

- **AWS OpsWorks for Puppet Enterprise** :
AWS OpsWorks for Puppet Enterprise is a fully managed configuration management service that hosts Puppet Enterprise.

- **AWS OpsWorks Stacks** : 
AWS OpsWorks Stacks is an application and server management service. With OpsWorks Stacks, you can model your application as a stack containing different layers, such as load balancing, database, and application server.



## Scenario
The following procedures help you easily host an Apache web server with PHP and MySQL support on your Amazon Linux instance. You can use this server to host website or deploy a dynamic application that reads and writes information to a database.

<p align="center">
    <img src="/images/42.png" width="50%" height="50%">
</p>


## Prerequisites
- The workshop’s region will be in ‘N.Virginia’
- Download [CloudFormation-Network.yml](deploy-your-cloudformation-template_CloudFormation-Network.yml)
- You can review ClourFormation templates via text editor like VS Code or NotePad++...etc.


## Lab tutorial

### Deploy CloudFormation-VPC 
- On the **Service** menu, select **CloudFormation**.

- Select **Create stack** button.

<p align="center">
    <img src="images/46.png" width="85%" height="85%">
</p>

- In **Prerequisite - Prepare template**, select **Template is ready**.

- In **Specify template**, select **Upload a template file** and click **choose file**.

- Upload **CloudFormation-Network.yml**, which downloaded in the prerequisites section.

<p align="center">
    <img src="images/43.jpg" width="85%" height="85%">
</p>

- Select **Next** to go on.


- In **Stack name**, input `Your Name` and leave other settings as default, click **Next** button.

<p align="center">
    <img src="images/44.png" width="85%" height="85%">
</p>

- Select **Next** to go on. We don't need to change any settings here.

- Select **Create stack**.


- Select **Your Name** in the list and click **Events** tab, wait until **Lab-Stack** shows **CREATE_COMPLETE** in status.

<p align="center">
    <img src="images/45.jpg" width="85%" height="85%">
</p>

> The resources inside the template will be created in a few minutes.



### Create Your Stack

- On the **Service** menu, click **OpsWorks**.

- Click **Add your first stack**.

<p align="center">
    <img src="/images/1.png" width="85%" height="85%">
</p>

- Select **Chef 11 stack**, and input the followings : 
    * Stack name : `PHP_website Your name`
    * Region : `US East (N.Virginia)`
    * VPC : `Lab VPC`
    * Default subnet : `Lab Public subnet`
    * Default operating system : `Default`

<p align="center">
    <img src="/images/2.png" width="85%" height="85%">
</p>

<p align="center">
    <img src="/images/3.png" width="85%" height="85%">
</p>

- Click **Add stack**.

- In the navigation pane, click **Layers**.

- Click **Add layer**.

- Enter the following details, and click **Add layer** :
    * Layer type : `PHP App Server`
    * ELB : `Default `

<p align="center">
    <img src="/images/4.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Instances**. 

- In **PHP App Server layer**, click **Add an instance**, and enter the following details:
    * Hostname : `php-app1-Your name`
    * Size : `t2.micro`
    * Subnet : `Lab Public subnet`
    

<p align="center">
    <img src="/images/5.png" width="85%" height="85%">
</p>

> Hostname needs lower case

- In **Layers** page, click **Network**

<p align="center">
    <img src="images/47.png" width="85%" height="85%">
</p>

- In **Automatically Assign IP Addresses**, enter the following details and click **save**:
    * Public IP addresses : `yes`

<p align="center">
    <img src="images/48.png" width="85%" height="85%">
</p>


- In the navigation pane, click **Instances**.

- In **PHP App Server**, click **Add an instance**.

- In **Actions** column, click **start**.

<p align="center">
    <img src="/images/33.png" width="85%" height="85%">
</p>

- Few minutes later, you will see your **status** is online.

<p align="center">
    <img src="/images/6.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Apps**.

- Click **Add app**.

- Enter the following details and click **Add App** :  
    * Name: `Your name`
    * Data source type : `None`
    * Repository type : `Git`
    * Repository URL : `git://github.com/awslabs/opsworks-demo-php-simple-app.git`
    * Branch/Revision : `version1`

<p align="center">
    <img src="/images/7.png" width="85%" height="85%">
</p>

- In the same page, in **Actions** column, click **Deploy**.

<p align="center">
    <img src="/images/8.png" width="85%" height="85%">
</p>

- Enter the following details and click **Deploy** :  
    * Command : `Deploy`
    * Comment : `None`

<p align="center">
    <img src="/images/9.png" width="85%" height="85%">
</p>

- Few minutes later, you can see **Status** is successful.

<p align="center">
    <img src="/images/10.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Instances**. Check your PHP App Server's **Public IP**.

<p align="center">
    <img src="/images/11.png" width="85%" height="85%">
</p>

- Copy **Public IP** to your browser, you can see the PHP website page. 

<p align="center">
    <img src="/images/12.png" width="85%" height="85%">
</p>


### Create MySQL

- In the navigation pane, click **Layers**. 

- Click **Add layer** 

- Enter the following details and click **Add Layer** : 
    * Layer type : `MySQL`
    * MySQL root user password : `Default`
    * Set root user password on every instance : `Yes`

<p align="center">
    <img src="/images/13.png" width="85%" height="85%">
</p>


- Click **Network**

<p align="center">
    <img src="images/49.png" width="85%" height="85%">
</p>

- In **Automatically Assign IP Addresses**, enter the following details and click **save**:
    * Public IP addresses : `yes`
<p align="center">
    <img src="images/50.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Layers**

- In **MySQL Layer**, click **Add instance**.

<p align="center">
    <img src="/images/14.png" width="85%" height="85%">
</p>

- Enter the following details and click **Add instance** :  
    * Name : `php-db-Your name`
    * Size : `t2.mirco`
    * Subnet : `Lab Public subnet`

<p align="center">
    <img src="/images/15.png" width="85%" height="85%">
</p>

> Name needs lower case

- In the navigation pane, click **Apps**.

- In **Actions** column, click **edit**.

- Enter the following details and click **Save** : 
    * Document root : `web`
    * Data source type : `OpsWorks` 
    * Vranch/Revision : `version2`

<p align="center">
    <img src="/images/27.png" width="85%" height="85%">
</p>


- In the navigation pane, click **Instances**. 

- In MySQL Layer,  click **Start**, then few minutes later, you can see **Status** is online

<p align="center">
    <img src="/images/36.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Stack**. click **Stack Settings**.

<p align="center">
    <img src="/images/20.png" width="85%" height="85%">
</p>

- Click **Edit**.

<p align="center">
    <img src="/images/35.png" width="85%" height="85%">
</p>

- Enter the following details and click **Save** : 
    * Use custom Chef cookbooks : `Yes`
    * Repository type : `Git`
    * Repository URL : `git://github.com/amazonwebservices/opsworks-example-cookbooks.git`

<p align="center">
    <img src="/images/21.png" width="85%" height="85%">
</p>

- In the navigation pane, click **Layers**. 

- In PHP App Server, click **Recipes**.

<p align="center">
    <img src="/images/24.png" width="85%" height="85%">
</p>

- In Custom Chef Recipes, enter the following details, and click **Save**
    * Deploy : `phpapp::appsetup`

<p align="center">
    <img src="/images/22.png" width="85%" height="85%">
</p>

- In MySQL, click **Recipes**.

<p align="center">
    <img src="/images/41.png" width="85%" height="85%">
</p>

- In Custom Chef Recipes, enter the following details, and click **Save**.
    * Deploy : `phpapp::dbsetup`

<p align="center">
    <img src="/images/23.png" width="85%" height="85%">
</p>


- In the navigation pane, click **Apps**. 

- In Actions, click **deploy**.

<p align="center">
    <img src="/images/25.png" width="85%" height="85%">
</p>


- Click **deploy**.

<p align="center">
    <img src="/images/26.png" width="85%" height="85%">
</p>

- Few minutes later, you can see **Status** is successful.

<p align="center">
    <img src="/images/40.png" width="85%" height="85%">
</p>




### Test your website

- In the navigation pane, click **Instances**. you can check your **Public IP** , then copy to the browser. 

<p align="center">
    <img src="/images/28.png" width="85%" height="85%">
</p>

- You can see the website, and click **Share Your Thought!**.

<p align="center">
    <img src="/images/29.png" width="85%" height="85%">
</p>

- Write something in that field, and click **Submit Yout Thought**.

<p align="center">
    <img src="/images/30.png" width="85%" height="85%">
</p>

- You can see success message, then click **Go Back**.

<p align="center">
    <img src="/images/31.png" width="85%" height="85%">
</p>

- You can see what you wrote. 

<p align="center">
    <img src="/images/32.png" width="85%" height="85%">
</p>



## Conclusion
Congratulations! You now have learned how to host a website in AWS. You can use OpsWorks easily to delpoy a website, check your instance healthy and use GitHub's code to delpoy APP to your layer.



## Clean up 
- clean Instances. 
- clean APPs.
- clean Layers.
- clean Stacks.
- clean CloudFormation
- clean EBS (for MySQL database)




## Appendix
* https://docs.aws.amazon.com/zh_tw/opsworks/latest/userguide/chef-11-linux.html
* https://aws.amazon.com/tw/opsworks/stacks/
